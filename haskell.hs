factorial :: Integer -> Integer
factorial 0 = 1
factorial n = n * factorial (n - 1)

-- Main function to print the factorial of 5
main :: IO ()
main = print (factorial 5)